function createCard(name, description, pictureUrl, startDate, endDate, location) {
    return `
    <div class="col grid gap-0 column-gap-3 row-gap-3">
        <div class="card shadow p-3 g-col-6 mb-5 bg-body-tertiary rounded ">
            <img src="${pictureUrl}" class="card-img-top">
            <div class="card-body">
                <h5 class="card-title">${name}</h5>
                <h6 class="card-subtitle mb-2 text-muted">${location}</h6>
                <p class="card-text">${description}</p>
            </div>
            <div class="card-footer">
            ${startDate} - ${endDate}
          </div>

          <div class="card" aria-hidden="true">
          <img src="..." class="card-img-top" alt="...">
          <div class="card-body">
            <h5 class="card-title placeholder-glow">
              <span class="placeholder col-6"></span>
            </h5>
            <p class="card-text placeholder-glow">
              <span class="placeholder col-7"></span>
              <span class="placeholder col-4"></span>
              <span class="placeholder col-4"></span>
              <span class="placeholder col-6"></span>
              <span class="placeholder col-8"></span>
            </p>
            <a href="#" tabindex="-1" class="btn btn-primary disabled placeholder col-6"></a>
          </div>
        </div>
    `;
  }


window.addEventListener('DOMContentLoaded', async () => {

    const url = 'http://localhost:8000/api/conferences/';

    try {
        const response = await fetch(url);

        if (!response.ok) {
            console.error("400 Bad Request")
            // figure out what to do when the response is bad
        } else {
            const data = await response.json();


      for (let conference of data.conferences) {
        const detailUrl = `http://localhost:8000${conference.href}`;
        const detailResponse = await fetch(detailUrl);
        if (detailResponse.ok) {
          const details = await detailResponse.json();
          const title = details.conference.name;
          const description = details.conference.description;
          const pictureUrl = details.conference.location.picture_url;
          const starts = new Date (details.conference.starts);
          const startDate = starts.toLocaleDateString();

          const ends = new Date (details.conference.ends);
          const endDate = ends.toLocaleDateString();

          const location = details.conference.location.name;

          const html = createCard(title, description, pictureUrl, startDate, endDate, location);
          const column = document.querySelector('#conference-cards');
          column.innerHTML += html;




        }
      }
    }



} catch (e) {
    console.error(e);
    // Figure out what to do if an error is raised
  }

});
