window.addEventListener('DOMContentLoaded', async () => {

    const url = 'http://localhost:8000/api/locations/';

    const response = await fetch(url);

    if (response.ok) {
      const data = await response.json();

      const locationTag = document.getElementById('location');
      for (let location of data.locations) {
        let option = document.createElement("option");
        option.value = location.id;
        option.innerHTML = location.name;
        locationTag.appendChild(option)
        // Create an 'option' element

        // Set the '.value' property of the option element to the
        // state's abbreviation

        // Set the '.innerHTML' property of the option element to
        // the state's name

        // Append the option element as a child of the select tag
      }
    }



    const formTag = document.getElementById('create-conference-form');
    formTag.addEventListener('submit', async event => {
        event.preventDefault();
        const formData = new FormData(formTag);
        const json = JSON.stringify(Object.fromEntries(formData));
        const conferenceUrl = 'http://localhost:8000/api/conferences/';
        const fetchConfig = {
            method: "post",
            body: json,
            headers: {
            'Content-Type': 'application/json',
        },
        };
        const conferenceResponse = await fetch(conferenceUrl, fetchConfig);
        if (conferenceResponse.ok) {
            formTag.reset();
            const newConference = await conferenceResponse.json();
            console.log(newConference);
        }



    });




  });
