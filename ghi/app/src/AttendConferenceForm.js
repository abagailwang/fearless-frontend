import React, {useEffect, useState} from 'react';

function AttendeesForm() {
    const [conferences, setConferences] = useState([]);
    const [conference, setConference] = useState('');
    const [name, setName] = useState('');
    const [email, setEmail] = useState('');

    const handleConferenceChange = (event) => {
        const value = event.target.value;
        setConference(value);
    }

    const handleNameChange = (event) => {
        const value = event.target.value;
        setName(value);
    }

    const handleEmailChange = (event) => {
        const value = event.target.value;
        setEmail(value);
    }


    const handleSubmit = async (event) => {
        event.preventDefault();
        //create an empty JSON object
        const data = {};


        data.name = name;
        data.email = email;
        data.conference = conference;

        console.log(data);

        const attendeeUrl = 'http://localhost:8001/api/attendees/';
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };

        const response = await fetch(attendeeUrl, fetchConfig);
        if (response.ok) {
            const newAttendee = await response.json();
            console.log(newAttendee);

            // promises:
            setName('');
            setEmail('');
            setConference('');
        }

    }







    const fetchData = async () => {
        const url = 'http://localhost:8000/api/conferences/';
        const response = await fetch(url);

        if (response.ok) {
          const data = await response.json();
          setConferences(data.conferences);

          // Here, add the 'd-none' class to the loading icon
          const loadTag = document.getElementById('loading-conference-spinner')
          loadTag.classList.add("d-none");
            const optionTag = document.getElementById('conference');

          // Here, remove the 'd-none' class from the select tag
          optionTag.classList.remove("d-none");

        }


    }

    useEffect(() => {
        fetchData();
      }, []);


return (
  <div className="my-5 container">
      <div className="row">
        <div className="col col-sm-auto">
          <img width="300" className="bg-white rounded shadow d-block mx-auto mb-4" src="/logo.svg" />
        </div>
        <div className="col">
          <div className="card shadow">
            <div className="card-body">
              <form onSubmit={handleSubmit} id="create-attendee-form">
                <h1 className="card-title">It's Conference Time!</h1>
                <p className="mb-3">
                  Please choose which conference
                  you'd like to attend.
                </p>
                <div className="d-flex justify-content-center mb-3" id="loading-conference-spinner">
                  <div className="spinner-grow text-secondary" role="status">
                    <span className="visually-hidden">Loading...</span>
                  </div>
                </div>
                <div className="mb-3">
                  <select onChange={handleConferenceChange} value={conference} id="conference" name="conference" className="form-select d-none" required>
                    <option value="">Choose a conference</option>
                    {conferences.map((conference, idx) => {
                    return (
                        <option key={conference.name + idx} value={conference.href}>
                            {/* treat the value as it is in insomnia/js/create attendees
                            treat the key as something react can use, since there are multiple
                            conferences with the same exact name in my data base, we should refer to idx to make it unique  */}
                        {conference.name}
                        </option>
                        );
                    })}
                  </select>
                </div>
                <p className="mb-3">
                  Now, tell us about yourself.
                </p>
                <div className="row">
                  <div className="col">
                    <div className="form-floating mb-3">
                      <input onChange={handleNameChange} value={name} required placeholder="Your full name" type="text" id="name" name="name" className="form-control" />
                      <label htmlFor="name">Your full name</label>
                    </div>
                  </div>
                  <div className="col">
                    <div className="form-floating mb-3">
                      <input onChange={handleEmailChange} value={email} required placeholder="Your email address" type="email" id="email" name="email" className="form-control" />
                      <label htmlFor="email">Your email address</label>
                    </div>
                  </div>
                </div>
                <button className="btn btn-lg btn-primary">I'm going!</button>
              </form>
              <div className="alert alert-success d-none mb-0" id="success-message">
                Congratulations! You're all signed up!
              </div>
            </div>
          </div>
        </div>
       </div>
    </div>
);

}

export default AttendeesForm;
