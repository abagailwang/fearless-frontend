import {
  BrowserRouter,
  Routes,
  Route,
} from 'react-router-dom';
import Nav from './Nav';
import MainPage from './MainPage';
import AttendeesList from './AttendeesList';
import LocationForm from './LocationForm';
import ConferenceForm from './ConferenceForm';
import AttendConferenceForm from './AttendConferenceForm';
import PresentationForm from './PresentationForm';
import { useState, useEffect } from "react";

function App(props) {
  // if (props.attendees === undefined) {
  //   return null;
  // }

  const [attendees, setAttendees] = useState([])
  const [locations, setLocations] = useState([])
  const [conferences, setConferences] = useState([])

  const fetchAttendees = async () => {
    const url = 'http://localhost:8001/api/attendees/'
    const response = await fetch(url);
      if (response.ok) {
        const data = await response.json();
        const attendees = data.attendees
        setAttendees(attendees)
      }
  }

  const fetchLocations = async () => {
    const url = 'http://localhost:8000/api/locations/'
    const response = await fetch(url);
      if (response.ok) {
        const data = await response.json();
        const locations = data.locations
        setLocations(locations)
      }
  }

  const fetchConferences = async () => {
    const url = 'http://localhost:8000/api/conferences/'
    const response = await fetch(url);
      if (response.ok) {
        const data = await response.json();
        const conferences = data.conferences
        setConferences(conferences)
      }
  }



  useEffect(() => {
    fetchAttendees();
    fetchLocations();
    fetchConferences();

  }, [])







  return (
    <BrowserRouter>
      <Nav />
        <div className="container">

          <Routes>

            <Route path="/" element={<MainPage locations={locations} fetchLocations={fetchLocations}/>} />


            <Route path="/attendees">
              <Route index element={<AttendeesList attendees={attendees} fetchAttendees={fetchAttendees}/>} />
              <Route path='new' element={<AttendConferenceForm attendees={attendees} fetchAttendees={fetchAttendees}/>} />
            </Route>

            <Route path="locations">
               <Route path="new" element={<LocationForm locations={locations} fetchLocations={fetchLocations} />} />
               </Route>


            <Route path="/conferences/new" element={<ConferenceForm fetchConferences={fetchConferences} conferences={conferences} />} />


            <Route path="/presentations/new" element={<PresentationForm />} />



          </Routes>
        </div>
    </BrowserRouter>
      );

    }

    export default App;
